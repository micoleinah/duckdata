﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DuckServer.Data;
using DuckServer.Models;

namespace DuckServer.Controllers
{
    [Produces("application/json")]
    [Route("api/DuckFeed")]
    [EnableCors("AllowAllOrigins")]
    public class DuckFeedApiController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DuckFeedApiController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/DuckFeedApi
        [HttpGet]
        public IEnumerable<Object> GetDuckFeed()
        {
            List<Object> feedData = new List<object>();
            foreach (DuckFeed s in _context.DuckFeed)
            {
                dynamic b = new ExpandoObject();

                b.country = s.Country;
                b.park = s.ParkName;
                b.numberOfDucks = s.NumberOfDucks;
                b.time = s.Time;
                b.foodItem = s.FoodItem;
                b.typeFood = s.TypeOfFoodItem;
                b.amountFood = s.AmountOfFood;
                b.mass = s.Mass;

                feedData.Add(s);
            }
            return feedData;
        }

        // GET: api/DuckFeedApi/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDuckFeed([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var duckFeed = await _context.DuckFeed.FindAsync(id);

            if (duckFeed == null)
            {
                return NotFound();
            }

            return Ok(duckFeed);
        }

        // PUT: api/DuckFeedApi/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDuckFeed([FromRoute] string id, [FromBody] DuckFeed duckFeed)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != duckFeed.Country)
            {
                return BadRequest();
            }

            _context.Entry(duckFeed).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DuckFeedExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/DuckFeedApi
        [HttpPost]
        public async Task<IActionResult> PostDuckFeed([FromBody] DuckFeed duckFeed)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.DuckFeed.Add(duckFeed);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDuckFeed", duckFeed);
        }

        // DELETE: api/DuckFeedApi/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDuckFeed([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var duckFeed = await _context.DuckFeed.FindAsync(id);
            if (duckFeed == null)
            {
                return NotFound();
            }

            _context.DuckFeed.Remove(duckFeed);
            await _context.SaveChangesAsync();

            return Ok(duckFeed);
        }

        private bool DuckFeedExists(string id)
        {
            return _context.DuckFeed.Any(e => e.Country == id);
        }
    }
}