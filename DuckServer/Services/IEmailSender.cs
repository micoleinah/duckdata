﻿using System.Threading.Tasks;

namespace DuckServer.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
