﻿using DuckServer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuckServer.Data
{
    public class SeedData
    {
        public static async Task Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new ApplicationDbContext(
                serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>()))
            {
                var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                if (!context.Users.Any(u => u.UserName == "admin"))
                {
                    var adminUser = new ApplicationUser
                    {
                        UserName = "admin",
                        FirstName = "Scientist",
                        LastName = "Admin"
                    };

                    await userManager.CreateAsync(adminUser, "P@$$w0rd");
                }

                InitializeFeedStats(context);
            }
        }

        public static void InitializeFeedStats(ApplicationDbContext db)
        {
            if (db.DuckFeed.Any())
            {
                return;
            }

            List<DuckFeed> duckData = new List<DuckFeed>
            {
                new DuckFeed
                {
                    Country = "USA",
                    ParkName = "Cuyahoga Valley National Park",
                    NumberOfDucks = 8,
                    Time = "12:45",
                    FoodItem = "Corn",
                    TypeOfFoodItem = "Vegetables",
                    AmountOfFood = 1,
                    Mass = "cup"
                },
                new DuckFeed
                {
                    Country = "Japan",
                    ParkName = "Nakajima Park",
                    NumberOfDucks = 20,
                    Time = "14:30",
                    FoodItem = "Peas",
                    TypeOfFoodItem = "Vegetables",
                    AmountOfFood = 0.5,
                    Mass = "lb"
                }
            };

            db.DuckFeed.AddRange(duckData);
            db.SaveChanges();
        }
    }
}
