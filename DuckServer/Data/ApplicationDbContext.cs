﻿using System;
using System.Collections.Generic;
using System.Text;
using DuckServer.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DuckServer.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<DuckServer.Models.DuckFeed> DuckFeed { get; set; }
        public DbSet<DuckServer.Models.ApplicationUser> ApplicationUser { get; set; }
    }
}
