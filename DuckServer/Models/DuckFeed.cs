﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DuckServer.Models
{
    public class DuckFeed
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Country { get; set; }
        [Display(Name = "Park Name")]
        public string ParkName { get; set; }
        [Display(Name = "Number of Ducks")]
        public int NumberOfDucks { get; set; }
        public string Time { get; set; }
        [Display(Name = "Food Item")]
        public string FoodItem { get; set; }
        [Display(Name = "Type of Food Item")]
        public string TypeOfFoodItem { get; set; }
        [Display(Name = "Amount of Food")]
        [DisplayFormat(DataFormatString = "{0:0.#}")]
        public double AmountOfFood { get; set; }
        public string Mass { get; set; }
    }
}
