import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MatFormField, MatFormFieldModule } from '@angular/material';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AccountService } from './services/account.service';
import { NewDataComponent } from './new-data/new-data.component';
import { FeedDataComponent } from './feed-data/feed-data.component';
import { DataService } from './services/data.service';
import { NgbTimepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

const appRoutes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'newData',
    component: NewDataComponent
  },
  {
    path: 'feedData',
    component: FeedDataComponent
  },
  {
    path: '',
    redirectTo: '/newData',
    pathMatch: 'full'
  },
  { 
    path: '**',
    component: NewDataComponent
  }
]
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NewDataComponent,
    FeedDataComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MatNativeDateModule,
    MatDatepickerModule,
    NgbTimepickerModule,
    MatInputModule,
    BrowserAnimationsModule
  ],
  providers: [AccountService, DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
