import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';
import { FeedData } from '../models/feed-data.model';

@Component({
  selector: 'app-new-data',
  templateUrl: './new-data.component.html',
  styleUrls: ['./new-data.component.css']
})
export class NewDataComponent implements OnInit {

  public country: string;
  public parkName: string;
  public numberOfDucks: number;
  public time: any;
  public foodItem: string;
  public typeOfFood: string[];
  public amountOfFood: number;
  public mass: string[];
  public selectedType: string;
  public selectedMass: string;
  public newTime:string;
  public error: string;
  

  constructor(private dataService: DataService, private router: Router) { 
    this.country = "";
    this.parkName = "";
    this.numberOfDucks = null;
    this.time = {hour: 0o0, minute: 0o0};
    this.foodItem = "";
    this.typeOfFood = [];
    this.amountOfFood = null;
    this.mass = [];
    this.selectedMass = "";
    this.selectedType = "";
    this.newTime = "";
    this.error = "";
  }

  ngOnInit() {
    this.typeOfFood = new Array("Carbohydrates", "Protein", "Vegetables", "Fruits", "Others");
    this.mass = new Array("g", "lb", "cup");
  }

  submit() {
    if(this.selectedMass == "")
      this.error = "Cannot be blank";
    
    let data: FeedData = new FeedData();

    data.country = this.country;
    data.parkName = this.parkName;
    data.numberOfDucks = this.numberOfDucks;
    data.time = this.newTime;
    data.foodItem = this.foodItem;
    data.typeOfFood = this.selectedType;
    data.amountOfFood = this.amountOfFood;
    data.mass = this.selectedMass;

    console.log(data);
    this.dataService.createNewData(data).subscribe(r => {
      this.router.navigate(["/feedData/"]);
    })
    
  }

  onTimeChange(value: {hour: string, minute: string})
  {
    console.log(value)
    this.newTime = `${value.hour}:${value.minute}`;
  }

}
