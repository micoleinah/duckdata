export class FeedData {
    public country: string;
    public parkName: string;
    public numberOfDucks: number;
    public time: string;
    public foodItem: string;
    public typeOfFood: string;
    public amountOfFood: number;
    public mass: string;
}