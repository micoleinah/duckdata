import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { FeedData } from '../models/feed-data.model';
import { AccountService } from './account.service';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable()
export class DataService {
    private url: string;
    private token: string;
    public data: FeedData[];
    public feedData: FeedData;
    

    constructor(private httpClient: HttpClient, private accountService: AccountService) {
        this.url = environment.url + "DuckFeed";
        this.token = accountService.token;

    }

    public getFeedData(): Observable<FeedData[]> {
        
        const httpOptions = {
            headers: new HttpHeaders({'Authorization': 'Bearer ' + this.token})
        };

        return this.httpClient.get<FeedData[]>(this.url, httpOptions).pipe(
            map((data: FeedData[]) => { return data; }),
            tap((data: FeedData[]) =>  this.data = data,
            catchError(this.handleError))
        );
    }

    public createNewData(data: FeedData): Observable<FeedData> {
        const httpOptions = {
            headers: new HttpHeaders({'Authorization': 'Bearer ' + this.token})
        };

        return this.httpClient.post<FeedData>(this.url, data, {}).pipe(
            map((feedData: FeedData) => this.feedData = feedData),
            catchError(this.handleError)
        );
    }

    private handleError(error: HttpErrorResponse): Observable<any> {
        if(error.error instanceof ErrorEvent) {
            console.log(" An error occured: ", error.error.message);
            return throwError(error.error.message);
        }
    }
}