import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { FeedData } from '../models/feed-data.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-feed-data',
  templateUrl: './feed-data.component.html',
  styleUrls: ['./feed-data.component.css']
})
export class FeedDataComponent implements OnInit {

  public feedData: FeedData[];

  constructor(private dataService: DataService) { }

  public ngOnInit(): void {
    this.dataService.getFeedData()
      .subscribe(
        (feedData: FeedData[]) => {
          this.feedData = feedData;
          console.log(this.feedData);
        }
      );
    //this.feedData = this.dataService.getFeedData();
  }
}
